#!/bin/bash

sleep 10

###########
# HOSTNAME
###########
vmtoolsd --cmd "info-get guestinfo.ovfEnv" > /OVFINFO.TXT
export NEWHOSTNAME=`vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep 'hostname' | awk '{print $3}' | cut -d \" -f2`
export DOCKER=`cat OVFINFO.TXT | grep 'V_DOCKER' | awk '{print $3}' | cut -d \" -f2`
export K8S=`cat OVFINFO.TXT | grep 'V_K8S' | awk '{print $3}' | cut -d \" -f2`
export CIDR=`cat OVFINFO.TXT | grep 'V_CIDR' | awk '{print $3}' | cut -d \" -f2`
export HAPROXY=`cat OVFINFO.TXT | grep 'V_HAPROXY_IP' | awk '{print $3}' | cut -d \" -f2`
export ETCD1=`cat OVFINFO.TXT | grep 'V_ETCD_IP_1' | awk '{print $3}' | cut -d \" -f2`
export ETCD2=`cat OVFINFO.TXT | grep 'V_ETCD_IP_2' | awk '{print $3}' | cut -d \" -f2`
export ETCD3=`cat OVFINFO.TXT | grep 'V_ETCD_IP_3' | awk '{print $3}' | cut -d \" -f2`
export ETCDHOST1=`cat OVFINFO.TXT | grep 'V_ETCD_HOST_1' | awk '{print $3}' | cut -d \" -f2`
export ETCDHOST2=`cat OVFINFO.TXT | grep 'V_ETCD_HOST_2' | awk '{print $3}' | cut -d \" -f2`
export ETCDHOST3=`cat OVFINFO.TXT | grep 'V_ETCD_HOST_3' | awk '{print $3}' | cut -d \" -f2`
export ETCD_CRT=`cat OVFINFO.TXT | grep 'V_ETCD_CA_CRT' | awk '{print $3}' | cut -d \" -f2`
export ETCD_KEY=`cat OVFINFO.TXT | grep 'V_ETCD_CA_KEY' | awk '{print $3}' | cut -d \" -f2`
export MASTER1=`cat OVFINFO.TXT | grep 'V_MASTER_IP_1' | awk '{print $3}' | cut -d \" -f2`
export MASTER2=`cat OVFINFO.TXT | grep 'V_MASTER_IP_2' | awk '{print $3}' | cut -d \" -f2`
export MASTER3=`cat OVFINFO.TXT | grep 'V_MASTER_IP_3' | awk '{print $3}' | cut -d \" -f2`
export THIS_IP=`hostname -I | awk '{print $1}'`


cat > /remove.sh <<EOF
kubeadm reset -f
apt-get remove -y docker-ce kubeadm kubelet kubectl
EOF
sudo chmod 0777 /remove.sh

rm -rf /config
mkdir /config
/remove.sh



#apt-get remove -y docker-ce kubeadm kubelet kubectl


hostnamectl set-hostname ${NEWHOSTNAME}
sed -i -e "s/ubuntu/${NEWHOSTNAME}/g" /etc/hosts


# Install Docker CE
## Set up the repository:
### Install packages to allow apt to use a repository over HTTPS
apt-get update && apt-get install -y apt-transport-https ca-certificates curl software-properties-common

### Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

### Add Docker apt repository.
add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

## Install Docker CE.
apt-get update && apt-get install -y docker-ce=${DOCKER}

# Setup daemon.
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

mkdir -p /etc/systemd/system/docker.service.d

# Restart docker.
systemctl daemon-reload
systemctl restart docker



apt-get update && apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet=${K8S}-00 kubeadm=${K8S}-00 kubectl=${K8S}-00

rm -rf /etc/kuberneted/pki
mkdir -p /etc/kubernetes/pki/etcd



curl -sSL https://gitlab.com/moondev/ovainit/raw/master/ca.crt > /etc/kubernetes/pki/etcd/ca.crt
curl -sSL https://gitlab.com/moondev/ovainit/raw/master/ca.key > /etc/kubernetes/pki/etcd/ca.key


sysctl net.bridge.bridge-nf-call-iptables=1
sysctl net.bridge.bridge-nf-call-ip6tables=1





cat > /config/master-ext-etcd.yaml <<EOF
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
kubernetesVersion: stable
controlPlaneEndpoint: "${HAPROXY}:6443"
etcd:
    external:
        endpoints:
        - https://${ETCD1}:2379
        - https://${ETCD2}:2379
        - https://${ETCD3}:2379
        caFile: /etc/kubernetes/pki/etcd/ca.crt
        certFile: /etc/kubernetes/pki/apiserver-etcd-client.crt
        keyFile: /etc/kubernetes/pki/apiserver-etcd-client.key
networking:
    podSubnet: "192.168.0.0/16"
EOF

cat > /config/haproxy.cfg <<EOF

global
defaults
	timeout client		30s
	timeout server		30s
	timeout connect		30s

frontend k8s-api
	bind			0.0.0.0:6443
	mode			tcp
	default_backend		k8s-api

backend k8s-api
   mode tcp
   option tcp-check
   balance roundrobin
   default-server inter 10s downinter 5s rise 2 fall 2 slowstart 60s maxconn 250 maxqueue 256 weight 100

       server apiserver1 ${MASTER1}:6443 check
       server apiserver2 ${MASTER2}:6443 check
       server apiserver3 ${MASTER3}:6443 check

listen stats # Define a listen section called "stats"
  bind 0.0.0.0:9000 # Listen on localhost:9000
  mode http
  stats enable  # Enable stats page
  stats hide-version  # Hide HAProxy version
  stats realm Haproxy\ Statistics  # Title text for popup window
  stats uri /  # Stats URI

EOF

cat > /config/start-haproxy.sh <<EOF
docker kill haproxy
docker rm haproxy
docker run --name haproxy -d  -p 1936:1936 -p 6443:6443 -p 9000:9000 -v /config/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg haproxy:1.9.4
EOF

chmod +x /config/start-haproxy.sh

cat > /config/20-etcd-service-manager.conf << EOF
[Service]
ExecStart=
ExecStart=/usr/bin/kubelet --cgroup-driver=systemd --address=127.0.0.1 --pod-manifest-path=/etc/kubernetes/manifests
Restart=always
EOF

cat > /etcdctl.sh << EOF
ETCDCTL_API=3 etcdctl --endpoints=https://192.168.1.148:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/peer.crt --key=/etc/kubernetes/pki/etcd/peer.key member list
EOF
chmod +x /etcdctl.sh

cat > /config/etcd.yaml << EOF
apiVersion: "kubeadm.k8s.io/v1beta2"
kind: ClusterConfiguration
etcd:
    local:
        serverCertSANs:
        - "${NEWHOSTNAME}"
        peerCertSANs:
        - "${NEWHOSTNAME}"
        extraArgs:
            initial-cluster: ${ETCDHOST1}=https://${ETCD1}:2380,${ETCDHOST2}=https://${ETCD2}:2380,${ETCDHOST3}=https://${ETCD3}:2380
            initial-cluster-state: new
            name: ${NEWHOSTNAME}
            listen-peer-urls: https://${THIS_IP}:2380
            listen-client-urls: https://${THIS_IP}:2379
            advertise-client-urls: https://${THIS_IP}:2379
            initial-advertise-peer-urls: https://${THIS_IP}:2380
EOF


cat > /config/init-etcd.sh <<EOF
rm -rf /var/lib/etcd
rm /etc/systemd/system/kubelet.service.d/*
cp /config/20-etcd-service-manager.conf /etc/systemd/system/kubelet.service.d/20-etcd-service-manager.conf
systemctl daemon-reload
systemctl restart kubelet
sleep 10
kubeadm init phase certs etcd-server --config=/config/etcd.yaml
kubeadm init phase certs etcd-peer --config=//config/etcd.yaml
kubeadm init phase certs etcd-healthcheck-client --config=/config/etcd.yaml
kubeadm init phase certs apiserver-etcd-client --config=/config/etcd.yaml
kubeadm init phase etcd local --config=/config/etcd.yaml
EOF
sudo chmod 0777 /config/init-etcd.sh

cat > /config/init-master.sh <<EOF
kubeadm init --config /config/master-ext-etcd.yaml --upload-certs
sleep 10
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl apply -f https://docs.projectcalico.org/v3.8/manifests/calico.yaml
EOF
chmod +x /config/init-master.sh


if [ "$NEWHOSTNAME" == "etcd-1" ]; then
  /config/init-etcd.sh
fi

if [ "$NEWHOSTNAME" == "etcd-2" ]; then
  /config/init-etcd.sh
fi

if [ "$NEWHOSTNAME" == "etcd-3" ]; then
  /config/init-etcd.sh
fi

if [ "$NEWHOSTNAME" == "haproxy" ]; then
  /config/start-haproxy.sh
fi


cd /config
wget https://github.com/etcd-io/etcd/releases/download/v3.3.13/etcd-v3.3.13-linux-amd64.tar.gz
tar -xf etcd-v3.3.13-linux-amd64.tar.gz
cp etcd-v3.3.13-linux-amd64/etcd /usr/local/bin/etcd
cp etcd-v3.3.13-linux-amd64/etcdctl /usr/local/bin/etcdctl
